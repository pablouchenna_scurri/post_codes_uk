#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'post_codes_UK_django_project.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


def runcommand(commands):
    """Run administrative tasks. To be used for setuptools entry_points"""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'post_codes_UK_django_project.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    print("""Run command. {}""".format(sys.argv))
    sys.argv.extend(commands)
    print("""Run command. {}""".format(sys.argv))
    execute_from_command_line(sys.argv)


def runserver():
    """Run server. To be used for setuptools entry_points"""
    print("""Run server. {}""".format(sys.argv))
    runcommand(['runserver'])


def test():
    """Run test. To be used for setuptools entry_points"""
    print("""Run test. {}""".format(sys.argv))
    runcommand(['test', 'post_codes_UK_app/tests'])


if __name__ == '__main__':
    main()
