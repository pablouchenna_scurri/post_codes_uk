from post_codes_UK_app.models import PostCodesUK, PostCodesUKResponse
from rest_framework.views import APIView
from rest_framework.response import Response  # Renders to content type as requested by the client.
from rest_framework import status  # explicit identifiers for each status code


class PostCodesUKFormatter(PostCodesUKResponse, APIView):
    """
**Validate** a UK Post Code .
------
**Validate** a UK post code following the following regular expression:

     ^
     ([Gg][Ii][Rr] 0[Aa]{2})
    |((  ([A-Za-z][0-9]{1,2})
        |(  ([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})
           |(  ([A-Za-z][0-9][A-Za-z])
              |([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])
            )
         )
      )
      [ ]
      [0-9][A-Za-z]{2}
     )$

Logic
-----
A combination of upper and lower case characters is allowed.
The length is determined by the regular expression and is between 6 and 8 characters.

      "GIR 0AA"
    OR
      One letter followed by either one or two numbers
    OR
      One letter followed by a second letter that must be one of
      ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
      and then followed by either one or two numbers
    OR
      One letter followed by one number and then another letter
    OR
      A two part post code where
        the first part must be
          One letter followed by a second letter that must be one of
          ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
          and then followed by one number and optionally a further letter after that
       AND
        The second part (separated by a space from the first part)
          must be One number followed by two letters.
"""

    response_template = {'post_code': '', 'post_code_formatted': ''}

    def get(self, request, post_code):
        post_code_formatted = PostCodesUK.format(post_code)
        if post_code_formatted:
            return Response(self.get_response({'post_code': post_code,
                                               'post_code_formatted': post_code_formatted}))
        return Response(self.get_response({'post_code': post_code,
                                           'post_code_formatted': ''}),
                        status=status.HTTP_422_UNPROCESSABLE_ENTITY)


class PostCodesUKValidator(PostCodesUKResponse, APIView):
    """
**Validate** a UK Post Code .
------
**Validate** a UK post code following the following regular expression:

     ^
     ([Gg][Ii][Rr] 0[Aa]{2})
    |((  ([A-Za-z][0-9]{1,2})
        |(  ([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})
           |(  ([A-Za-z][0-9][A-Za-z])
              |([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])
            )
         )
      )
      [ ]
      [0-9][A-Za-z]{2}
     )$

Logic
-----
A combination of upper and lower case characters is allowed.
The length is determined by the regular expression and is between 6 and 8 characters.

      "GIR 0AA"
    OR
      One letter followed by either one or two numbers
    OR
      One letter followed by a second letter that must be one of
      ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
      and then followed by either one or two numbers
    OR
      One letter followed by one number and then another letter
    OR
      A two part post code where
        the first part must be
          One letter followed by a second letter that must be one of
          ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
          and then followed by one number and optionally a further letter after that
       AND
        The second part (separated by a space from the first part)
          must be One number followed by two letters.
"""

    def get(self, request, post_code):
        post_code_formated = PostCodesUK.format(post_code)
        if post_code_formated and PostCodesUK.validate(post_code_formated):
            return Response(self.get_response({'post_code': post_code,
                                               'is_valid': True}))
        return Response(self.get_response({'post_code': post_code,
                                           'is_valid': False}),
                        status=status.HTTP_422_UNPROCESSABLE_ENTITY)
