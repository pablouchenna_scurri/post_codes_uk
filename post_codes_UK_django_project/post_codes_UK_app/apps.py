from django.apps import AppConfig


class PostCodesUkAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'post_codes_UK_app'
