from django.urls import path

from post_codes_UK_app import views

urlpatterns = [
    path('validate/<str:post_code>/',
         views.PostCodesUKValidator.as_view(),
         name='post-codes-uk-validator'),
    path('format/<str:post_code>/',
         views.PostCodesUKFormatter.as_view(),
         name='post-codes-uk-formatter'),
]
