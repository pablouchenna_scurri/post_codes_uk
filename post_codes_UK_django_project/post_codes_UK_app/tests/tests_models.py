from django.test import TestCase

from post_codes_UK_app.models import PostCodesUKResponse
from post_codes_UK_app.models import PostCodesUK
from post_codes_UK_app.tests.data import tests_models_data as data


class PostCodesUKResponseTests(TestCase):

    def setUp(self):
        self.base_response_template = {'post_code': ''}

    def test_empty_update(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertDictEqual(PostCodesUKResponse.get_response({}),
                             self.base_response_template)

    def test_update(self):
        """
        Test we get the proper response when updating with a populated dict.
        """
        update_fields = {'f1': 'f1_key',
                         'f2': 'f2_key', }
        expected_response = self.base_response_template.copy()
        expected_response.update(update_fields)
        self.assertDictEqual(PostCodesUKResponse.get_response(update_fields),
                             expected_response)


class PostCodesUKTests(TestCase):

    def setUp(self):
        self.base_response_template = {'post_code': ''}
        self.valid_whitespace_post_code = '  \t gir \t 0AA  \t '

    def test_remove_whitespace(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertEqual(PostCodesUK.remove_whitespace(self.valid_whitespace_post_code),
                         'gir0AA')

    def test_format_too_small(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertIs(PostCodesUK.format('  \t gir \t 0  \t '),
                      None)

    def test_format_too_big(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertIs(PostCodesUK.format('  \t gir \t 0AAAAA  \t '),
                      None)

    def test_format_min_length(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertEqual(PostCodesUK.format('  \t gir \t 0A  \t '),
                         'gi r0A')

    def test_format_max_length(self):
        """
        Test we get the default response when updating with empty.
        """
        self.assertEqual(PostCodesUK.format('  \t gir \t 0AAA  \t '),
                         'gir0 AAA')

    def test_validate_true(self):
        """
        Test valid post codes getting a meaningful message if the test fails.

        See data/tests_models_data.py
        """
        for test_case in data.validate_true_codes:
            self.assertTrue(PostCodesUK.validate(test_case[0]), test_case[1])

    def test_validate_false(self):
        """
        Test invalid post codes getting a meaningful message if the test fails.

        See data/tests_models_data.py
        """
        for test_case in data.validate_false_codes:
            self.assertFalse(PostCodesUK.validate(test_case[0]), test_case[1])

    '''
    The following tests are covered on
      test_validate_true
      test_validate_false

    They were done earlier.
    I find test_validate_true and test_validate_false
    easier to develop, more clear, readable and maintainable.

    See data/tests_models_data.py
    '''

    def test_validate_bad_format(self):
        """
        Test a valid post code with bad format.
        """
        self.assertFalse(PostCodesUK.validate(self.valid_whitespace_post_code))

    def test_validate_gir_0AA(self):
        """
        Test a valid post code 'gir 0AA'.
        """
        self.assertTrue(PostCodesUK.validate('gir 0AA'))

    def test_validate_letter_number_0AA(self):
        """
        Test a valid post code .
        """
        self.assertTrue(PostCodesUK.validate('z0 0AA'))

    def test_validate_letter_number_number_0AA(self):
        """
        Test a valid post code .
        """
        self.assertTrue(PostCodesUK.validate('z00 0AA'))

    def test_validate_letter_letter_number_0AA(self):
        """
        Test a valid post code letter letter(but i z) number 0AA
        """
        self.assertTrue(PostCodesUK.validate('ab0 0AA'))

    def test_validate_letter_letter_number_number_0AA(self):
        """
        Test a valid post code letter letter(but i z) number number 0AA
        """
        self.assertTrue(PostCodesUK.validate('aa09 0AA'))

    def test_validate_letter_iz_number_0AA(self):
        """
        Test an invalid post code letter i|z number 0AA
        """
        self.assertFalse(PostCodesUK.validate('ai0 0AA'))
        self.assertFalse(PostCodesUK.validate('az0 0AA'))

    def test_validate_letter_iz_number_number_0AA(self):
        """
        Test an invalid post code letter i|z number number 0AA
        """
        self.assertFalse(PostCodesUK.validate('ai09 0AA'))
        self.assertFalse(PostCodesUK.validate('az09 0AA'))

    def test_validate_letter_number_letter_0AA(self):
        """
        Test a valid post code letter number letter 0AA
        """
        self.assertTrue(PostCodesUK.validate('a0z 0AA'))

    def test_validate_letter_letter_letter_0AA(self):
        """
        Test a valid post code letter letter(but i z) letter 0AA
        """
        self.assertTrue(PostCodesUK.validate('abz 0AA'))

    def test_validate_letter_letter_number_letter_0AA(self):
        """
        Test a valid post code letter letter(but i z) number letter 0AA
        """
        self.assertTrue(PostCodesUK.validate('ab0z 0AA'))

    def test_validate_letter_iz_letter_0AA(self):
        """
        Test an invalid post code letter i|z letter 0AA
        """
        self.assertFalse(PostCodesUK.validate('aiz 0AA'))
        self.assertFalse(PostCodesUK.validate('azz 0AA'))

    def test_validate_letter_iz_number_letter_0AA(self):
        """
        Test an invalid post code letter i|z number letter 0AA
        """
        self.assertFalse(PostCodesUK.validate('ai0z 0AA'))
        self.assertFalse(PostCodesUK.validate('az0z 0AA'))

    def test_validate_letter_not_letter_number_number_0AA(self):
        """
        Test an invalid post code letter not_letter not_number number 0AA
        """
        self.assertFalse(PostCodesUK.validate('a000 0AA'))

    def test_validate_letter_letter_not_number_number_0AA(self):
        """
        Test an invalid post code letter letter not_number number 0AA
        """
        self.assertFalse(PostCodesUK.validate('aba0 0AA'))

    def test_validate_final_too_big(self):
        """
        Test an invalid post code gir 0AAA.
        """
        self.assertFalse(PostCodesUK.validate('gir 0AAA'))

    def test_validate_final_too_small(self):
        """
        Test an invalid post code gir 0A.
        """
        self.assertFalse(PostCodesUK.validate('gir 0A'))

    def test_validate_initial_number(self):
        """
        Test an invalid post code starting with a number 0ir 0AA
        """
        self.assertFalse(PostCodesUK.validate('0ir 0AA'))

    def test_validate_last_part_not_initial_number(self):
        """
        Test an invalid post code last part not starting with a number gir AAA
        """
        self.assertFalse(PostCodesUK.validate('gir AAA'))

    def test_validate_last_part_number_not_letter_letter(self):
        """
        Test an invalid post code last part with a number in the middle gir 00A
        """
        self.assertFalse(PostCodesUK.validate('gir 00A'))

    def test_validate_last_part_number_letter_not_letter(self):
        """
        Test an invalid post code last part with a number in the endgir 0A0.
        """
        self.assertFalse(PostCodesUK.validate('gir 0A0'))
