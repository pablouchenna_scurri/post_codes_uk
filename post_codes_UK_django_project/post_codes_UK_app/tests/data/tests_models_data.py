"""
Created on 29 Apr 2021

Create True and False tests for each line of pattern_post_code

Based on the definition at https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting :

pattern_post_code = '''
# This regular expression is meant to be user with the flags "re.IGNORECASE | re.VERBOSE"
   ^                                      # start of the string
   (
     ([g][i][r][ ]0[a]{2})                 # gir space 0aa
    |((  ([a-z][0-9]{1,2})                 # or letter number number(optional)
        |(  ([a-z][a-hj-y][0-9]{1,2})      # or letter letter(but i z) number number(optional)
           |(  ([a-z][0-9][a-z])           # or letter number letter
              |([a-z][a-hj-y][0-9]?[a-z])  # or letter letter(but i z) number(optional) letter
            )
         )
      )
      [ ]                                  # and space
      [0-9][a-z]{2}                        # and number letter letter
     )
   )
   $                                      # end of the string
'''

Conventions (IGNORECASE):
  a = [a-z]
  0 = [0-9]
  @ = [a-hj-y]

Valid post code abstraction :
  "first_part space second_part"

Valid first_part if:
  'gir'
  0 length:
    not valid
  1 length:
    not valid
  2 length:
    'a0'
  3 length:
    'a00'
    'a0a'
    'a@0'
    'a@a'
  4 length:
    'a@00'
    'a@0a'

Valid first_part if:
  '0aa'

@author: pablo
"""


_true_codes_tests = (('gir 0aa', '{} should be the only valid expression with i as the second character'),
                     ('a0 0aa', '{} should be valid, pattern ([a-z][0-9]{{1,2}})'),
                     ('z09 0aa', '{} should be valid, pattern ([a-z][0-9]{{1,2}})'),
                     ('ah0 0aa', '{} should be valid, pattern ([a-z][a-hj-y][0-9]{{1,2}})'),
                     ('jy09 0aa', '{} should be valid, pattern ([a-z][a-hj-y][0-9]{{1,2}})'),
                     ('a0z 0aa', '{} should be valid, pattern ([a-z][0-9][a-z])'),
                     ('z9a 0aa', '{} should be valid, pattern ([a-z][0-9][a-z])'),
                     ('ahz 0aa', '{} should be valid, pattern ([a-z][a-hj-y][0-9]?[a-z])'),
                     ('zj0a 0aa', '{} should be valid, pattern ([a-z][a-hj-y][0-9]?[a-z])'),
                     )

validate_true_codes = tuple((x[0], x[1].format(x[0])) for x in _true_codes_tests)


_false_codes_tests = ((' gir 0aa', '{} should be invalid, space at the beginning'),
                      # first part length 0
                      ('', '{} should be invalid, empty string'),
                      (' 0aa', '{} should be invalid, empty string'),
                      # first part length 1
                      ('z 0aa', '{} should be invalid, only 1 character before space'),
                      # first part length 2
                      ('aa 0aa', '{} should be invalid, 2 characters before space but the second in not a number'),
                      # first part length 3
                      ('0ar 0aa', '{} should be invalid, no letter at the beginning'),
                      ('aia 0aa', '{} should be invalid, i on 2 position except for gir 0aa'),
                      ('gzr 0aa', '{} should be invalid, z on 2 position'),
                      # first part length 4
                      ('aiaa 0aa', '{} should be invalid, i on 2 position except for gir 0aa'),
                      ('gzra 0aa', '{} should be invalid, z on 2 position'),
                      # first part length 5 or more
                      ('aaaaa 0aa', '{} should be invalid, first part length 5 or more'),
                      ('aa0aa 0aa', '{} should be invalid, first part length 5 or more'),
                      ('aa00a 0aa', '{} should be invalid, first part length 5 or more'),
                      ('aa000 0aa', '{} should be invalid, first part length 5 or more'),
                      # second part length 2 or less
                      ('a00 ', '{} should be invalid, second part length 2 or less'),
                      ('a00 0', '{} should be invalid, second part length 2 or less'),
                      ('a00 0a', '{} should be invalid, second part length 2 or less'),
                      # second part length 4 or more
                      ('a00 0aaa', '{} should be invalid, second part length 4 or more'),
                      ('a00 0aa0', '{} should be invalid, second part length 4 or more'),
                      # space at the end
                      ('gir 0aa ', '{} should be invalid, space at the end'),
                      )

validate_false_codes = tuple((x[0], x[1].format(x[0])) for x in _false_codes_tests)
