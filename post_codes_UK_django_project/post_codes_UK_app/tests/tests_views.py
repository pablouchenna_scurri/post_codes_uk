from django.test import TestCase
from django.test import Client


class PostCodesUKFormatterTests(TestCase):

    def setUp(self):
        self.url_template = '/post_codes_UK_app/format/{}/'
        self.c = Client()

#         self.base_response_template = {'post_code': ''}

    def test_valid_post_code(self):
        """
        Test the valid post code gir 0aa.
        """
        url = self.url_template.format('gir 0aa')
        response = self.c.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual('{"post_code":"gir 0aa","post_code_formatted":"gir 0aa"}',
                             response.json())

    def test_valid_extra_space(self):
        """
        Test a valid post code with spaces in between '    g   i  r   0 a  a    '.
        """
        valid_whitespace_post_code = '    g   i  r   0 a  a    '
        url = self.url_template.format(valid_whitespace_post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual('{{"post_code":"{}","post_code_formatted":"gir 0aa"}}'.format(valid_whitespace_post_code),
                             response.json())

    def test_too_small(self):
        """
        Test an invalid post code too small.
        """
        post_code = 'a 0aa'
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 422)
        self.assertJSONEqual('{{"post_code":"{}","post_code_formatted":""}}'.format(post_code),
                             response.json())

    def test_too_big(self):
        """
        Test an invalid post code too big.
        """
        post_code = 'aa00 0aaa'
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 422)
        self.assertJSONEqual('{{"post_code":"{}","post_code_formatted":""}}'.format(post_code),
                             response.json())


class PostCodesUKValidatorTests(TestCase):

    def setUp(self):
        self.url_template = '/post_codes_UK_app/validate/{}/'
        self.c = Client()

    def test_valid(self):
        post_code = 'gir 0aa'
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual('{{"post_code":"{}","is_valid":true}}'.format(post_code),
                             response.json())

    def test_valid_not_formatted(self):
        post_code = ' g i r  0 a a   '
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertJSONEqual('{{"post_code":"{}","is_valid":true}}'.format(post_code),
                             response.json())

    def test_too_small(self):
        post_code = 'a 0aa'
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 422)
        self.assertJSONEqual('{{"post_code":"{}","is_valid":false}}'.format(post_code),
                             response.json())

    def test_too_big(self):
        post_code = 'aa00 0aaa'
        url = self.url_template.format(post_code)
        response = self.c.get(url)
        self.assertEquals(response.status_code, 422)
        self.assertJSONEqual('{{"post_code":"{}","is_valid":false}}'.format(post_code),
                             response.json())
