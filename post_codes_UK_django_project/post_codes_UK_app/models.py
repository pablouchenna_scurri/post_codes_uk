import re


class PostCodesUKResponse():
    '''
    Get a proper API response.

    Initially developed to be used on view.py
    '''
    response_template = {'post_code': ''}

    @classmethod
    def get_response(cls, fields):
        response = cls.response_template.copy()
        response.update(fields)
        return response


class PostCodesUK(object):
    '''
    Format and validate a UK post code following the following regular expression:

    ^
    ([Gg][Ii][Rr] 0[Aa]{2})
   |((  ([A-Za-z][0-9]{1,2})
       |(  ([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})
          |(  ([A-Za-z][0-9][A-Za-z])
             |([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])
           )
        )
     )
     [ ]
     [0-9][A-Za-z]{2}
    )$

    Logic
    -----
    A combination of upper and lower case characters is allowed.
    The length is determined by the regular expression and is between 6 and 8 characters.

      "GIR 0AA"
    OR
      One letter followed by either one or two numbers
    OR
      One letter followed by a second letter that must be one of
      ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
      and then followed by either one or two numbers
    OR
      One letter followed by one number and then another letter
    OR
      A two part post code where
        the first part must be
          One letter followed by a second letter that must be one of
          ABCDEFGHJKLMNOPQRSTUVWXY (i.e..not I or Z)
          and then followed by one number and optionally a further letter after that
       AND
        The second part (separated by a space from the first part)
          must be One number followed by two letters.
    '''

    post_code_help_message = '''
A combination of upper and lower case characters is allowed.

The length is determined by the regular expression and is between 6 and 8 characters.

    Start with one of the following:
    - gir space 0aa
    - or letter number number(optional)
    - or letter letter(but i z) number number(optional)
    - or letter number letter
    - or letter letter(but i z) number(optional) letter

    Then:
    - space

    End with:
    - number letter letter(optional)

Valid post codes examples:
    EC1A 1BB
    W1A 0AX
    M1 1AE
    B33 8TH
    CR2 6XH
    DN55 1PT
    GIR 0AA
'''
    re_flags = re.IGNORECASE | re.VERBOSE
    # flags for these pattern_post_code: "re.IGNORECASE | re.VERBOSE"
    pattern_post_code = re.compile('''
    # This regular expression is meant to be user with the flags "re.IGNORECASE | re.VERBOSE"
       ^                                      # start of the string
       (
         ([g][i][r][ ]0[a]{2})                 # gir space 0aa
        |((  ([a-z][0-9]{1,2})                 # or letter number number(optional)
            |(  ([a-z][a-hj-y][0-9]{1,2})      # or letter letter(but i z) number number(optional)
               |(  ([a-z][0-9][a-z])           # or letter number letter
                  |([a-z][a-hj-y][0-9]?[a-z])  # or letter letter(but i z) number(optional) letter
                )
             )
          )
          [ ]                                  # and space
          [0-9][a-z]{2}                        # and number letter letter
         )
       )
       $                                      # end of the string
    ''',
                                   re_flags)
    # Any whitespace characters; this is equivalent to the class [ \t\n\r\f\v]+
    pattern_whitespace = re.compile(r'\s+')

    def __init__(self, params):
        '''
        Constructor
        '''

    @staticmethod
    def remove_whitespace(post_code):
        return re.sub(PostCodesUK.pattern_whitespace, '', post_code)

    @staticmethod
    def format(post_code):
        '''
        Remove any extra whitespace,
        add the space in the middle
        and checks the length of post_code

        :param post_code: the post code to format

        :return post_code with no extra whitespace if it has the correct length
                None otherwise
        '''
        # remove all whitespace
        post_code = PostCodesUK.remove_whitespace(post_code)
        # insert space
        post_code = post_code[:-3] + ' ' + post_code[-3:]
        # post code length should be between 6 and 8 characters
        if (len(post_code) < 6) or (len(post_code) > 8):
            return
        return post_code

    @staticmethod
    def validate(post_code):
        '''
        Check that post_code match the validation regular expression

        :param post_code: the post code to validate

        :return True if post_code validation the regular expression
                False otherwise
        '''
        if not PostCodesUK.pattern_post_code.match(post_code):
            return False
        return True
