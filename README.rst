Post Codes UK
-------------
::

  Write a library that supports validating and formatting post codes for UK. 
  The details of which post codes are valid and which are the parts they consist of can be found at 
  https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting


Installation
------------

It requires python >= 3.9

Go to the installation directory of you choice.

To work in a virtual environment you may use:
::

  $ pip install --user pipenv
  $ pipenv --python 3.9
  $ pipenv install
  $ pipenv shell  # activates the virtual environment


Use
----
::

  $ cd post_codes_UK_django_project
  $ python manage.py runserver

It will run at http://127.0.0.1:8000/
Examples:
::
  # Json
  http://127.0.0.1:8000/post_codes_UK_app/validate/gir%200aa%20%20/?format=json
  http://127.0.0.1:8000/post_codes_UK_app/format/gir%200aa%20%20/?format=json
  # html
  http://127.0.0.1:8000/post_codes_UK_app/validate/gir%200aa%20%20/
  http://127.0.0.1:8000/post_codes_UK_app/format/gir%200aa%20%20/

Tests
-----
::

  $ cd post_codes_UK_django_project
  $ python manage.py test -v 3 post_codes_UK_app/tests


Version
-------

BumpVersion can be used to change the version wherever it is needed


Issues
--------

*Just if you want to build a python package*
	For different reasons I had to install the following:
	::
	
	  sudo apt install libffi-dev -y
	  sudo apt-get install libssl-dev
	  sudo apt-get install zlib1g-dev
	  sudo apt install libsqlite3-dev
	
	And then recomplile python 3.9:
	::
	
	  make clean
	  ./configure
	  make
	  make test
	  sudo make altinstall


Comments
--------

Regards,
Pablo
